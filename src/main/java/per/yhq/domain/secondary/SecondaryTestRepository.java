package per.yhq.domain.secondary;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SecondaryTestRepository extends JpaRepository<Secondary, Long> {

}
