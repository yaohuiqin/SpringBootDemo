package per.yhq.domain.secondary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Secondary {
    @Id
    @GeneratedValue
    private Long secondaryId;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private int age;

    public Secondary() {
    }

    public Secondary(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Long getSecondaryId() {
        return secondaryId;
    }

    public void setSecondaryId(Long secondaryId) {
        this.secondaryId = secondaryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
