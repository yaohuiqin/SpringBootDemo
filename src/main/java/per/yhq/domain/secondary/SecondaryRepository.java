package per.yhq.domain.secondary;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SecondaryRepository  extends JpaRepository<Secondary, Long> {
}
