package per.yhq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexJspController {
    @RequestMapping(value = "/indexJsp",method = RequestMethod.GET)
    public String index(){
        return "indexjsp";
    }
}
