package per.yhq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import per.yhq.service.PropertiesTest;


@RestController
@RequestMapping
public class HelloWordController {
    @Autowired
    private PropertiesTest PropertiesTest;
    @RequestMapping(value = "/getName",method = RequestMethod.GET)
    public String index(){
        return PropertiesTest.getName();
    }
}
