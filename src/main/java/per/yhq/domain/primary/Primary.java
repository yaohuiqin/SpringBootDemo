package per.yhq.domain.primary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Primary implements Serializable {

    private static final long serialVersionUID = -66428858944015431L;
    @Id
    @GeneratedValue
    private Long primaryId;
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int age;

    public Primary(Long primaryId, String name, int age) {
        this.primaryId = primaryId;
        this.name = name;
        this.age = age;
    }

    public Primary() {
    }

    public Long getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(Long primaryId) {
        this.primaryId = primaryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Primary{" +
                "primaryId=" + primaryId +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
