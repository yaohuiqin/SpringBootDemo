package per.yhq.domain.primary;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PrimaryRepository extends JpaRepository<Primary, Long> {

}
