package per.yhq.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import per.yhq.domain.User;
import per.yhq.service.UserOperateService;
import java.util.List;
@Service
public class UserOperateServiceImpl implements UserOperateService {
    @Autowired
    @Qualifier("primaryJdbcTemplate")
    private JdbcTemplate primaryJdbcTemplate;
    @Autowired
    @Qualifier("secondaryJdbcTemplate")
    private JdbcTemplate secondaryJdbcTemplate;
    @Override
    public List<User> getAllUsersbyprimaryJdbcTemplate() {
        List list = primaryJdbcTemplate.queryForList("SELECT userId,NAME, AGE FROM tableuser");
        return list;
    }
    @Override
    public List<User> getAllUsersbysecondaryJdbcTemplate() {
        List list = secondaryJdbcTemplate.queryForList("SELECT userId,NAME, AGE FROM tableuser");
        return list;
    }
}
