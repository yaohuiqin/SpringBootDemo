package per.yhq.controller.springdataJpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import per.yhq.domain.secondary.Teacher;
import per.yhq.domain.secondary.TeacherRepository;

import java.util.List;

@RestController
@RequestMapping(value="/Teacher")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(value="/findByName")
    public Teacher findByName(String name){
        Teacher teacher = teacherRepository.findByName(name);
        return teacher;
    }

    @RequestMapping(value="/findAll")
    public List<Teacher> findAll(){
        List<Teacher> teacher=teacherRepository.findAll();
        return teacher;
    }

    @RequestMapping(value="/deleteById")
    public Teacher deleteUser(Long id){
        Teacher teacher = teacherRepository.findById(id);
        teacherRepository.deleteByTeacherId(id);
        return teacher;
    }


}
