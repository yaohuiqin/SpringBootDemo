package per.yhq.controller;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import per.yhq.domain.User;
import per.yhq.service.UserOperateService;
import per.yhq.serviceImpl.UserOperateServiceImpl;

import java.util.List;

@RestController
@RequestMapping(value = "/userOperateController")
public class UserOperateController {

    @Autowired
    UserOperateService userOperateService;

    @RequestMapping(value = "/getAllUsersBysecondaryJdbcTemplate")
    public List<User> getAllUsersBysecondaryJdbcTemplate(){
        return userOperateService.getAllUsersbysecondaryJdbcTemplate();
    }

    @RequestMapping(value = "/getAllUsersByprimaryJdbcTemplate")
    public List<User> getAllUsersByprimaryJdbcTemplate(){
        return userOperateService.getAllUsersbyprimaryJdbcTemplate();
    }
}
