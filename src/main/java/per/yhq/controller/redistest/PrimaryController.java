package per.yhq.controller.redistest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import per.yhq.domain.primary.Primary;
import per.yhq.domain.primary.PrimaryRepository;

import java.util.List;

@RestController
@RequestMapping(value="redisprimary")
public class PrimaryController {
    @Autowired
    PrimaryRepository repository;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate<String, Primary> redisTemplate;

    @RequestMapping("getall")
    public List<Primary> getAll(){

        stringRedisTemplate.opsForValue().set("stringset", "111");
        System.out.println(stringRedisTemplate.opsForValue().get("stringset"));
        System.out.println(redisTemplate.opsForValue().get("yy"));
        List<Primary> primaryList=repository.findAll();
        for (Primary p:primaryList){
            System.out.println(p);
        }
        Primary primary=primaryList.get(0);
        redisTemplate.opsForValue().set("yy", primary);
        System.out.println(primary);
        System.out.println(redisTemplate.opsForValue().get("yy"));
        return primaryList;
    }
}
