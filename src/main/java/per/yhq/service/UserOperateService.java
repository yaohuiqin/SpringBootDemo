package per.yhq.service;

import org.springframework.context.annotation.Bean;
import per.yhq.domain.User;

import java.util.List;

public interface UserOperateService {
    public List<User> getAllUsersbyprimaryJdbcTemplate();

    public List<User> getAllUsersbysecondaryJdbcTemplate();
}
