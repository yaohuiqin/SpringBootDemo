package per.yhq.domain.secondary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Teacher findByName(String name);

    Teacher findById(Long id);

    Teacher findByNameAndAge(String name, Integer age);

    Teacher save(Teacher teacher);

    @Transactional
    @Modifying
    @Query("delete from Teacher where id = ?1")
    void deleteByTeacherId(Long id);
}
